const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const path = require('path');
const multer = require('multer');
require('dotenv').config();

const app = express();
const PORT = process.env.PORT || 5000;

// Middleware
app.use(cors());
app.use(bodyParser.json());

// Multer configuration for file uploads
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, 'uploads/'); // Uploads directory
  },
  filename: function (req, file, cb) {
    cb(null, Date.now() + '-' + file.originalname); // File naming
  }
});
const upload = multer({ storage: storage });

// MongoDB connection
const dbURI = process.env.MONGODB_URI || 'mongodb://localhost:27017/crud_app';
mongoose.connect(dbURI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useCreateIndex: true
});
const connection = mongoose.connection;
connection.once('open', () => {
  console.log('MongoDB database connection established successfully');
});
connection.on('error', err => {
  console.error('MongoDB connection error:', err);
});

// Schemas and Models
const Schema = mongoose.Schema;

const loginSchema = new Schema({
  f_userName: { type: String, required: true },
  f_Pwd: { type: String, required: true },
});

const Login = mongoose.model('Login', loginSchema);

const employeeSchema = new Schema({
  f_Id: { type: String, required: true },
  f_Image: String,
  f_Name: { type: String, required: true },
  f_Email: String,
  f_Mobile: String,
  f_Designation: String,
  f_gender: String,
  f_Courses: [{ type: String }], 
  f_Createdate: { type: Date, default: Date.now },
});

const Employee = mongoose.model('Employee', employeeSchema);

// Routes

// Signup route (for creating new users)
app.post('/api/signup', async (req, res) => {
  const { f_userName, f_Pwd } = req.body;

  try {
    // Check if username already exists
    const existingUser = await Login.findOne({ f_userName });
    if (existingUser) {
      return res.status(400).json({ success: false, message: 'Username already exists' });
    }

    // Create a new user instance
    const newUser = new Login({ f_userName, f_Pwd });

    // Save the user to the database
    await newUser.save();

    res.json({ success: true, message: 'User registered successfully' });
  } catch (err) {
    // Handle errors
    console.error('Error signing up:', err);
    res.status(500).json({ success: false, message: 'Failed to register user' });
  }
});

// Create new employee route
app.post('/api/employees', upload.single('f_Image'), async (req, res) => {
  const { f_Id, f_Name, f_Email, f_Mobile, f_Designation, f_gender, f_Courses } = req.body;
  const f_Image = req.file ? req.file.path : null;

  const newEmployee = new Employee({
    f_Id,
    f_Name,
    f_Email,
    f_Mobile,
    f_Designation,
    f_gender,
    f_Courses: JSON.parse(f_Courses),
    f_Image,
  });

  try {
    await newEmployee.save();
    res.json({ success: true, message: 'Employee added successfully' });
  } catch (err) {
    res.status(500).json({ success: false, message: err.message });
  }
});

// Get all employees route
app.get('/api/employees', async (req, res) => {
  try {
    const employees = await Employee.find();
    res.json(employees);
  } catch (err) {
    res.status(500).json({ success: false, message: err.message });
  }
});

// Get single employee by ID route
app.get('/api/employees/:id', async (req, res) => {
  try {
    const employee = await Employee.findById(req.params.id);
    if (!employee) {
      return res.status(404).json({ success: false, message: 'Employee not found' });
    }
    res.json({ success: true, employee });
  } catch (err) {
    console.error('Error fetching employee:', err);
    res.status(500).json({ success: false, message: 'Server error' });
  }
});

// Update employee by ID route
app.put('/api/employees/:id', upload.single('f_Image'), async (req, res) => {
  const { f_Id, f_Name, f_Email, f_Mobile, f_Designation, f_gender, f_Courses } = req.body;
  const f_Image = req.file ? req.file.path : null;

  try {
    const updatedEmployee = await Employee.findById(req.params.id);

    if (!updatedEmployee) {
      return res.status(404).json({ success: false, message: 'Employee not found' });
    }

    // Update the employee fields
    updatedEmployee.f_Id = f_Id || updatedEmployee.f_Id;
    updatedEmployee.f_Name = f_Name || updatedEmployee.f_Name;
    updatedEmployee.f_Email = f_Email || updatedEmployee.f_Email;
    updatedEmployee.f_Mobile = f_Mobile || updatedEmployee.f_Mobile;
    updatedEmployee.f_Designation = f_Designation || updatedEmployee.f_Designation;
    updatedEmployee.f_gender = f_gender || updatedEmployee.f_gender;
    updatedEmployee.f_Courses = f_Courses ? JSON.parse(f_Courses) : updatedEmployee.f_Courses;
    if (f_Image) {
      updatedEmployee.f_Image = f_Image;
    }

    await updatedEmployee.save();
    res.json({ success: true, message: 'Employee updated successfully' });
  } catch (err) {
    console.error('Error updating employee:', err);
    res.status(500).json({ success: false, message: 'Failed to update employee' });
  }
});

// Delete employee by ID route
app.delete('/api/employees/:id', async (req, res) => {
  try {
    const deletedEmployee = await Employee.findByIdAndDelete(req.params.id);

    if (!deletedEmployee) {
      return res.status(404).json({ success: false, message: 'Employee not found' });
    }

    res.json({ success: true, message: 'Employee deleted successfully' });
  } catch (err) {
    console.error('Error deleting employee:', err);
    res.status(500).json({ success: false, message: 'Failed to delete employee' });
  }
});

// Serve static assets in production (for deployment)
if (process.env.NODE_ENV === 'production') {
  app.use(express.static('frontend/build'));
  app.get('*', (req, res) => {
    res.sendFile(path.resolve(__dirname, 'frontend', 'build', 'index.html'));
  });
}

// Start server
app.listen(PORT, () => {
  console.log(`Server is running on port: ${PORT}`);
});
